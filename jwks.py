#!/usr/bin/python3

# A script to convert pem files to jwks

from jwcrypto import jwk
import os

ROOT = os.path.dirname(__file__)

KEY_NAME = "client2-pub.pem"

with open(os.path.join(ROOT, KEY_NAME), "rb") as pem:
    key = jwk.JWK.from_pem(pem.read())

with open(os.path.join(ROOT, 'out.json'), 'w') as out:
    out.write(key.export(private_key=False))
