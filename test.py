import requests
from pathlib import Path
from dotenv import load_dotenv

import yaml
import json
import os
from jwcrypto.jwk import JWK
from jwcrypto.jwt import JWT

from requests.auth import HTTPBasicAuth

ROOT = Path('.')
load_dotenv()

def retrieve_clients():
    with open(ROOT / 'confs/secret-keys.yml') as file:
        doc = yaml.load(file, Loader=yaml.SafeLoader)
    clients = json.loads(doc['stringData']['clients.json'])

    for c in clients:
        c['jwk'] = JWK.from_pem(c['private_key'].encode())
    return clients


CLIENTS = retrieve_clients()
USERNAME = "thega"
PASSWORD = "ageht"

GATEWAY_URL = "http://" + os.getenv("GATEWAY_URL")
AUTH_URL = GATEWAY_URL + '/auth/token'
HELLO_PATH = '/hello'
WORLD_PATH = '/world'
TRANSFER_PATH = '/transfer'

# ================================================================
# Calls

def client_credentials(client):
    return requests.post(
        AUTH_URL,
        data = {'grant_type': 'client_credentials'},
        auth=HTTPBasicAuth(client['id'], client['secret'])
    )

def password_credentials(client):
    return requests.post(
        AUTH_URL,
        data = {
            'grant_type': 'password',
            'username': USERNAME,
            'password': PASSWORD,
        },
        auth=HTTPBasicAuth(client['id'], client['secret'])
    )

def client_token(client):
    return client_credentials(client).json()['access_token']

def password_token(client):
    return password_credentials(client).json()['access_token']

def req(route, token, method=requests.get, **params):
    headers = {}
    if token:
        headers['Authorization'] = 'Bearer ' + token
    return method(GATEWAY_URL + route, headers=headers, **params)

def make_token(client, claims):
    jwt = JWT(header={"alg": "ES256", "typ": "JWT"},
              claims={**{"iss": "getiota.fr", "aud": client['slug']}, **claims})
    jwt.make_signed_token(client['jwk'])
    return jwt.serialize()

# ================================================================
# Tests
def test_no_auth():
    for route in HELLO_PATH, WORLD_PATH:
        r = req(route, token=None)
        assert r.status_code == 403
    print("Success: No auth requests disallowed")

def test_client_credentials():
    for c in CLIENTS:
        assert client_credentials(c).status_code == 200
    print("Success: Client auth")

def test_password_credentials():
    for c in CLIENTS:
        assert password_credentials(c).status_code == 200
    print("Success: Password auth")

def test_world():
    # World is accessible from any token with getiota.fr issuer
    for c in CLIENTS:
        t = make_token(c, {})
        r = req(WORLD_PATH, t)
        assert r.status_code == 200
    print("Success: World")

def test_hello():
    # World requires "role1" role
    for c in CLIENTS:
        assert req(HELLO_PATH, make_token(c, {"roles": ['role1']})).status_code == 200
        assert req(HELLO_PATH, make_token(c, {"roles": ['role2']})).status_code == 403
    print("Success: Hello")

def test_cross_client():
    # Trying to sign client2 token with client1 key
    token = make_token(CLIENTS[0], {'aud': 'client2'})
    assert req(HELLO_PATH, token).status_code == 403
    print("Success: cross client")

if __name__ == "__main__":
    test_no_auth()

    test_client_credentials()
    test_password_credentials()

    test_world()
    test_hello()

    test_cross_client()
