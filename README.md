# Hello k8s

A set of k8s configs to work with istio.

## Goals

### Setup Gateway to access apps

See [gateway.yml](./confs/gateway.yml)

### Requiring JWTs to access all routes except auth route

See [gateway-auth.yml](./confs/gateway-auth.yml)

#### Notes:
- rules apply to service in istio-system namespace
- a `RequestAuthentication` maps JWTs to their keys
- an `AuthorizationPolicy` allows traffic to the gateway with a valid JWT except for the auth route.
>>> Even though the auth route is excluded here, the policy will still deny all traffic to the route, even with a valid
>>> JWT. This is because of the way istio resolves policies:
>>> 1. If there are any DENY policies that match the request, deny the request.
>>> 2. If there are no ALLOW policies for the workload, allow the request.
>>> 3. If any of the ALLOW policies match the request, allow the request.
>>> 4. Deny the request. (this is where the auth route falls here)
- an `AuthorizationPolicy` allows traffic to the auth route without a JWT

### Deny all traffic between pods unless specified otherwise

>>> https://medium.com/better-programming/enable-access-control-between-your-kubernetes-workloads-using-istio-cf72a9f9bd5e

>>> find the gateway service account by running `kubectl get serviceaccount -n istio-system`

#### POC

- allow traffic coming to pods from the gateway
- build 1 app with two routes calling other routes in the cluster
- allow only 1 route

#### Notes:

See [security.yml](confs/security.yml)
1. Make default deny all policy
2. Use service accounts to uniquely identify pods and use the field in principals

### Multi clients

#### POC
- add another client to the auth-server and gateway auth
- have routes accessible by a client but not the other

#### Notes:

See [jwts.yml](confs/jwts.yml).

- adding a client to Istio is equivalent to adding a new JWT signature verification for the given audience or issuer
- it is just a RequestAuthentication and is decoupled from AuthorizationPolicy

### Role based authorization

#### POC
- add another user with a different role
- restrict access to a route with that role

#### Notes:

See [gateway-auth.yml](confs/gateway-auth.yml).

- A single file is enough for all ALLOW policies.
- There is no need for a default deny policy. Since there is an allow policy
applied to the workload (gateway), all requests not matched by a rule will be
rejected.

## POC Setup

- Run clean minikube server `minikube start`
- Install [istio](https://istio.io/latest/docs/setup/getting-started/) with demo profile
`istioctl install --set profile=demo`
- Add istio label: `kubectl label namespace default istio-injection=enabled`

### Getting cluster URL
```sh
export INGRESS_PORT=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="http2")].nodePort}')
export INGRESS_HOST=$(minikube ip)
export GATEWAY_URL=$INGRESS_HOST:$INGRESS_PORT
echo http://$GATEWAY_URL
```

## Notes
- Check that third party JWT auth is activated for istiod
- Check [best practices](https://istio.io/latest/docs/ops/best-practices/)
- Istio freaks out if the jwks structure is not right
- Istio disallows all requests if the sub is not present in the JWT

### Troubleshooting

To restart deployments: `kubectl rollout restart deployment`